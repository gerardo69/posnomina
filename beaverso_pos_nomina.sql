-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-07-2020 a las 17:34:00
-- Versión del servidor: 5.6.39-83.1
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `beaverso_pos_nomina`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backup`
--

CREATE TABLE `backup` (
  `backupid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `personalId` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backup`
--

INSERT INTO `backup` (`backupid`, `name`, `personalId`, `reg`, `activo`) VALUES
(1, 'Backup_pos_2019-06-04_09-14-10', 1, '2019-06-04 14:14:14', 1),
(2, 'Backup_pos_2019-06-04_09-20-44', 1, '2019-06-04 14:20:49', 1),
(3, 'Backup_pos_2019-06-04_09-33-01', 1, '2019-06-04 14:33:06', 1),
(4, 'Backup_pos_2019-06-04_09-34-06', 1, '2019-06-04 14:34:10', 1),
(5, 'Backup_pos_2019-06-04_11-49-10', 1, '2019-06-04 16:49:32', 1),
(6, 'Backup_pos_2019-06-04_17-11-08', 1, '2019-06-04 17:11:08', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `bitacoraid` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `id_cambio` int(11) NOT NULL,
  `tipo_cambio` varchar(200) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`, `reg`) VALUES
(1, 'Ninguno', 1, '2019-04-08 01:20:09'),
(2, 'Lacteos', 1, '2019-06-24 00:49:24'),
(3, 'Cereales', 1, '2019-06-24 01:14:35'),
(4, 'Aceite Comestible', 1, '2019-06-25 13:33:49'),
(5, 'Café y Té', 1, '2019-06-25 13:40:14'),
(6, 'Embutidos', 1, '2019-06-25 13:43:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` bigint(20) NOT NULL,
  `Nombre` varchar(250) NOT NULL,
  `Domicilio` text,
  `Ciudad` varchar(100) DEFAULT NULL,
  `Estado` varchar(100) DEFAULT NULL COMMENT 'Estado/Provincia/Region',
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(200) NOT NULL,
  `nombrec` varchar(100) NOT NULL,
  `correoc` varchar(200) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nombre`, `Domicilio`, `Ciudad`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `celular`, `descripcionc`, `activo`, `propietarioid`, `reg`) VALUES
(1, 'PUBLICO EN GENERAL', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', 1, 0, '2019-04-26 22:24:22'),
(2, 'DULCIBOTANASsx', 'GUADALAJARA', 'PUEBLA', 'PUEBLA', 'Mexico', '72150', 'edreimagdiel@gmail.com', 'EDREI', 'edreimagdiel@gmail.com', '2225464434', '', '', '', 1, 0, '2019-04-26 22:24:22'),
(3, 'xxxxxxxx11', 'calle', 'municipio', 'estado', 'mexico', '94140', 'ddd@h.com', 'contacto', 'ccc', '111111111', '11', '222222', 'des', 1, 0, '2019-04-26 22:24:22'),
(4, 'ñkñkñk', 'k', 'k', 'k', 'M?xico', '94140', 'cas@hot.com', 'n', 'cas@h.com', '123456789', '123', '123456', 'fghjk', 1, 0, '2019-04-26 22:24:22'),
(5, 'hectoe lagunes loyo', 'conocida', 'conocido', 'veracruz', 'M?xico', '94140', 'lagunes@hotmal.com', 'yo', 'lagunes@hotmal.com', '12356789', '34', '2345678', 'sd?okfs?ldkfsd', 1, 0, '2019-04-26 22:24:22'),
(6, 'l', 'l', 'l', 'l', NULL, 'l', 'magiicirqus@gmail.com', 'l', 'magiicirqus@gmail.com', 'l', 'l', 'l', '<ol>\r\n	<li>lsdfd<strong>fsdf</strong>sdf<em>sdfs</em></li>\r\n	<li><em>555555</em></li>\r\n</ol>\r\n', 1, 0, '2019-04-26 22:24:22'),
(7, 'Pablo Jimenez', 'sur 103', 'puebla', '', NULL, '', '', '', '', '', '', '', '', 1, 0, '2020-07-04 17:45:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `compraId` int(11) NOT NULL,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `id_proveedor` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `monto_total` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`compraId`, `sucursalid`, `id_proveedor`, `personalId`, `monto_total`, `activo`, `reg`) VALUES
(1, 1, 24, 1, 115, 1, '2019-05-14 12:44:40'),
(2, 1, 24, 1, 115, 1, '2019-05-14 12:46:34'),
(3, 1, 24, 1, 120, 1, '2019-05-14 12:48:41'),
(4, 1, 24, 1, 120, 1, '2019-05-14 12:50:01'),
(5, 1, 24, 1, 120, 1, '2019-05-14 12:55:25'),
(6, 1, 24, 1, 120, 1, '2019-05-14 13:02:59'),
(7, 1, 24, 1, 120, 1, '2019-05-14 13:03:59'),
(8, 1, 24, 1, 120, 1, '2019-05-14 13:07:44'),
(9, 1, 24, 1, 120, 1, '2019-05-14 13:07:58'),
(10, 2, 23, 1, 50, 1, '2019-08-25 14:13:11'),
(11, 1, 23, 1, 10, 1, '2020-04-12 21:31:41'),
(12, 1, 23, 1, 1650, 1, '2020-07-04 17:51:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_detalles`
--

CREATE TABLE `compras_detalles` (
  `compradId` int(11) NOT NULL,
  `compraId` int(11) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `compras_detalles`
--

INSERT INTO `compras_detalles` (`compradId`, `compraId`, `productoid`, `cantidad`, `precio_compra`) VALUES
(1, 1, 1, 5, 23),
(2, 2, 1, 5, 23),
(3, 3, 1, 6, 20),
(4, 4, 1, 6, 20),
(5, 5, 1, 6, 20),
(6, 6, 1, 6, 20),
(7, 7, 1, 6, 20),
(8, 8, 1, 6, 20),
(9, 9, 1, 6, 20),
(10, 10, 3, 100, 0.5),
(11, 11, 12, 1, 10),
(12, 12, 13, 30, 55);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `usuariosmaximo` int(2) NOT NULL DEFAULT '4',
  `productosmaximos` int(11) NOT NULL DEFAULT '4000',
  `vigencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `costomensual` float NOT NULL COMMENT '3% + 3 pesos',
  `costoanual` int(11) NOT NULL COMMENT '3% + 3 pesos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `usuariosmaximo`, `productosmaximos`, `vigencia`, `costomensual`, `costoanual`) VALUES
(1, 4, 4000, '2019-12-24 22:26:51', 240, 2578);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `gastosid` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` float NOT NULL,
  `concepto` text NOT NULL,
  `sucursal` int(11) NOT NULL,
  `comentarios` text NOT NULL,
  `personalId` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1' COMMENT '1 vigente 0 cancelado',
  `personalId_cancela` int(11) DEFAULT NULL,
  `motivo_cancela` text,
  `reg_cancela` timestamp NULL DEFAULT NULL,
  `activod` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`gastosid`, `fecha`, `cantidad`, `concepto`, `sucursal`, `comentarios`, `personalId`, `activo`, `personalId_cancela`, `motivo_cancela`, `reg_cancela`, `activod`, `reg`) VALUES
(1, '2019-05-25', 500.1, 'sss', 1, 'yyyy', 1, 0, 1, 'xxx', '2019-05-09 03:00:00', 1, '2019-05-09 05:03:42'),
(2, '2019-05-10', 333, 'xxx', 1, 'xxx', 1, 0, 3, 's', '2019-09-01 01:06:32', 1, '2019-05-10 16:00:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Pagina` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Personal', 'Personal', 'fa fa-group'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Productos', 'Productos', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Reportes', 'Reportes', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', '-', 'fa fa-cogs'),
(10, 2, 'Turno', '-', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', '-', 'fa fa-cogs'),
(12, 2, 'Lista de compras', '-', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 2, 'Verificador', 'Verificador', 'fa fa-eye'),
(15, 2, 'Gastos', 'Gastos', 'fa fa-money'),
(16, 1, 'Sucursal', 'Sucursal', 'fa fa-briefcase'),
(17, 2, 'Listado Ventas Normales', 'ListadoVentasNormales', 'fa fa-bar-chart'),
(18, 2, 'Listado Ventas Credito', 'ListadoVentasCredito', 'fa fa-bar-chart'),
(19, 3, 'Usuarios', 'Usuarios', 'fa fa-user'),
(20, 3, 'Pagos', 'Pagos', 'fa fa-cogs'),
(21, 3, 'Respaldos', 'Respaldos', 'fa fa-download');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_credito`
--

CREATE TABLE `pagos_credito` (
  `pagoId` int(11) NOT NULL,
  `ventaId` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `pago` float NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pagos_credito`
--

INSERT INTO `pagos_credito` (`pagoId`, `ventaId`, `personalId`, `pago`, `reg`) VALUES
(1, 5, 1, 10, '2019-05-14 03:22:15'),
(2, 5, 1, 10, '2019-05-14 03:23:28'),
(7, 5, 1, 1, '2019-05-16 11:31:33'),
(8, 5, 1, 2, '2019-05-16 11:31:52'),
(9, 5, 1, 1, '2019-05-16 11:32:17'),
(10, 6, 1, 10, '2019-05-27 01:32:30'),
(11, 6, 1, 15.5, '2019-05-27 01:33:44'),
(12, 6, 1, 38, '2019-05-27 01:34:20'),
(13, 7, 1, 44.6, '2019-05-27 14:14:06'),
(14, 8, 1, 100, '2019-05-29 13:52:05'),
(16, 8, 1, 100, '2019-05-29 14:00:27'),
(17, 8, 1, 1, '2019-06-01 20:40:34'),
(18, 8, 1, 4.2, '2019-06-01 20:43:28'),
(19, 6, 1, 1.3, '2019-06-01 20:43:48'),
(20, 11, 1, 10, '2019-06-02 22:51:40'),
(21, 31, 1, 100, '2020-07-04 17:47:36'),
(22, 31, 1, 230, '2020-07-04 17:48:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador root'),
(2, 'Administrador'),
(3, 'Ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 7),
(5, 1, 6),
(6, 1, 4),
(7, 1, 13),
(8, 1, 14),
(9, 1, 15),
(10, 1, 16),
(11, 1, 17),
(12, 1, 18),
(13, 1, 5),
(14, 1, 8),
(15, 1, 19),
(16, 1, 20),
(17, 2, 1),
(18, 2, 2),
(19, 2, 3),
(20, 2, 7),
(21, 2, 6),
(22, 2, 4),
(23, 2, 14),
(24, 2, 15),
(25, 2, 17),
(26, 2, 18),
(27, 2, 5),
(28, 2, 8),
(29, 2, 19),
(30, 3, 4),
(31, 3, 5),
(32, 3, 14),
(33, 3, 15),
(34, 1, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `domicilio` text NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `sucursalId` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `domicilio`, `telefono`, `celular`, `correo`, `tipo`, `sucursalId`, `activo`, `reg`) VALUES
(1, 'Administrador', '', '', '', '', 0, 1, 1, '2019-03-18 20:10:27'),
(2, 'gerardo', 'conocido', '123456789', '1234567890', 'soporte@mangoo.mx', 1, 1, 1, '2019-03-18 20:10:27'),
(3, 'vvvvx', 'fffffffffff', '3435453453', '345345345', '353@h.com', 1, 1, 1, '2019-03-25 01:44:40'),
(4, 'karen', 'av sur', '', '', '', 1, 2, 1, '2020-07-04 17:35:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` bigint(20) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `stock1` float NOT NULL DEFAULT '0',
  `stock2` float NOT NULL DEFAULT '0',
  `stock3` float NOT NULL DEFAULT '0',
  `preciocompra` float NOT NULL,
  `precioventa` float NOT NULL,
  `img` varchar(120) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `nombre`, `descripcion`, `categoria`, `stock1`, `stock2`, `stock3`, `preciocompra`, `precioventa`, `img`, `activo`, `propietarioid`, `reg`) VALUES
(1, '4567890', 'chetoss', 'chetos torcidos', 1, -5, 0, 0, 20, 24, '190421-160858catchetos.jpg', 1, 0, '2019-04-21 21:08:58'),
(2, '65546546', 'ñmñlmlk', 'lkmlk', 1, 11, 0, 0, 15, 15, '', 1, 0, '2019-05-13 03:20:27'),
(3, '75045623', 'Trident', 'chicle', 1, 100, 0, 0, 0.5, 1.5, '190623-204157cat0750610560605L.jpg', 1, 0, '2019-06-04 23:35:13'),
(4, '1235478595', 'chocokrispis', 'chocokrispis', 3, 30, 0, 0, 25, 30, '190623-201943catkicproduct.jpg', 1, 0, '2019-06-24 01:17:01'),
(5, '1235478595', 'chocokrispis', 'chocokrispis', 3, 30, 0, 0, 25, 30, '190623-201740catkicproduct.jpg', 1, 0, '2019-06-24 01:17:40'),
(6, '750102051534', 'Leche Lala Entera Ultrapast 1 Lt', 'Leche Lala Entera Ultrapast 1 Lt', 2, 100, 0, 0, 17, 19.9, '190625-082810catlala.jpg', 1, 0, '2019-06-25 13:28:10'),
(7, '750106050001', 'Aceite Mixto Patrona Bot 1 Lt', 'Aceite Comestible Mixto Patrona Botella De Plastico 1 Pieza De 1 Litro', 4, 100, 0, 0, 20, 23, '190625-083506cat750106050001-01-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:35:06'),
(8, '750102054066', 'Nutrileche Regular 1 Lt', 'Producto Lacteo Nutrileche Regular 1 Lt', 2, 100, 0, 0, 10, 16, '190625-083729cat7501020540666-00-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:37:29'),
(9, '750129560012', 'Leche Santa Clara Entera 1 Lt', 'Leche Santa Clara Entera 1 Lt\n', 2, 100, 0, 0, 15, 20.5, '190625-083921cat7501295600126-00-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:39:21'),
(10, '750105861770', 'Café Soluble Nestlé Nescafé Clasico 225', 'Café Soluble Nestlé Nescafé Clasico 225', 5, 100, 0, 0, 80, 87.9, '190625-084206cat7501058617705-00-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:42:06'),
(11, '750151846152', 'Salchicha Bafar Para Asar 800 Gr', 'Salchicha Bafar Para Asar 800 Gr\n', 6, 100, 0, 0, 40, 49, '190625-084526cat750151846152-00-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:45:26'),
(12, '750103233243', 'Licuado Danone Nuez Cereales 450g', 'Licuado Danone Nuez Cereales 450g\n', 2, 100, 0, 0, 10, 15.8, '190625-085011cat750103233243-00-CH300Wx300H.jpg', 1, 0, '2019-06-25 13:50:11'),
(13, '12345', 'costillas', 'de res', 1, 0, 0, 0, 55, 0, '', 1, 0, '2020-07-04 17:41:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_sucursales`
--

CREATE TABLE `productos_sucursales` (
  `id` int(11) NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `idproducto` bigint(20) NOT NULL,
  `precio_venta` float NOT NULL,
  `mayoreo` float NOT NULL,
  `can_mayoreo` float NOT NULL,
  `existencia` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos_sucursales`
--

INSERT INTO `productos_sucursales` (`id`, `idsucursal`, `idproducto`, `precio_venta`, `mayoreo`, `can_mayoreo`, `existencia`) VALUES
(1, 1, 12, 15.8, 14, 10, 93),
(2, 2, 12, 15.7, 14, 10, 93),
(3, 1, 11, 40, 35, 15, 100),
(4, 2, 11, 40, 35, 15, 100),
(5, 1, 10, 87.9, 87, 10, 99),
(6, 2, 10, 87.9, 87, 10, 99),
(7, 1, 9, 87, 85, 15, 100),
(8, 2, 9, 87, 85, 15, 100),
(9, 1, 8, 20, 19, 15, 100),
(10, 2, 8, 20, 19, 15, 100),
(11, 1, 7, 15, 14.5, 10, 100),
(12, 2, 7, 15, 14.5, 10, 100),
(13, 1, 6, 15, 15, 10, 100),
(14, 2, 6, 15, 15, 10, 100),
(15, 1, 4, 25, 24, 15, 92),
(16, 2, 4, 25, 24, 15, 92),
(17, 1, 3, 1.5, 1.1, 0, 196),
(18, 2, 3, 1.5, 0, 0, 105),
(19, 1, 2, 15, 14, 10, 100),
(20, 2, 2, 15, 14, 10, 100),
(21, 1, 1, 10, 9.5, 10, 100),
(22, 2, 1, 10, 9.5, 10, 100),
(23, 1, 13, 70, 65, 10, 42.5),
(24, 2, 13, 65, 60, 6, 32.5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) NOT NULL,
  `estado` varchar(200) NOT NULL COMMENT 'estado, provincia, etc',
  `cp` varchar(8) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(15) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `estado`, `cp`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `obser`, `activo`) VALUES
(23, 'BIMBO S.A DE C.V', 'AAA', 'AAA', '', '12345', '123', '123', 'AAA', 'AAA', '\0HERD890308UA', '', 1),
(24, 'DULCERIA SUSY S.A DE C.V. SUCURSAL 3', '104 PTE NO 1720-A LOC 10 COL. ', 'PUEBLA', 'puebla', '876546', '8765432345', '2227535616', 'HECTOR', 'm@gmail.com', 'DSU910312LSO', 'asdasdasdasdass', 1),
(25, 'laknsldknaklsdnlkasd', '', '', '', '', '', '', '', '', '', '', 1),
(26, 'razon social2', 'domicilio2', 'ciudad2', '', '94140', '2345678', '123456789', 'contacto', 'email', '\0rfc', 'obser', 0),
(27, 'razon', 'domi', 'ciudad', '', '94140', '2345678', '23456789', 'contacto', 'email', '\0rfc', 'obser', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `sucursalid` int(11) NOT NULL,
  `logo` text NOT NULL,
  `sucursal` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `direccion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`sucursalid`, `logo`, `sucursal`, `direccion`, `telefono`, `activo`) VALUES
(1, '190604-175529catlogo.png', 'sucursal1', 'CONOCIDA', '218-1184', 1),
(2, '', 'sucursal2', '', '', 1),
(3, '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `mensajea` text NOT NULL,
  `mensajeb` text NOT NULL,
  `fuente` varchar(20) NOT NULL,
  `margen_superior` int(11) NOT NULL,
  `tamanio` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `anchot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensajea`, `mensajeb`, `fuente`, `margen_superior`, `tamanio`, `barcode`, `anchot`) VALUES
(1, 'titulo', '<p>QUEJAS Y SUJERENCIAS 01 800 000 0000</p>\r\n', '<p>Como te atendimos?</p>\r\n\r\n<p>YA AHORRASTE CON LOS PRECIOS MAS BAJOS</p>\r\n', 'courier', 10, 9, 19, 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_bar`
--

CREATE TABLE `ticket_bar` (
  `barId` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket_bar`
--

INSERT INTO `ticket_bar` (`barId`, `nombre`, `codigo`, `activo`) VALUES
(1, 'CODABAR', 'CODABAR', 1),
(2, 'CODE 39 - ANSI MH10.8M-1983 - USD-3 - 3 of 9', 'C39', 1),
(3, 'CODE 39 + CHECKSUM', 'C39+', 1),
(4, 'CODE 39 EXTENDED', 'C39E', 1),
(5, 'CODE 39 EXTENDED + CHECKSUM', 'C39E+', 1),
(6, 'CODE 93 - USS-93', 'C93', 1),
(7, 'Standard 2 of 5', 'S25', 1),
(8, 'Standard 2 of 5 + CHECKSUM', 'S25+', 1),
(9, 'Interleaved 2 of 5', 'I25', 1),
(10, 'Interleaved 2 of 5 + CHECKSUM', 'I25+', 1),
(11, 'CODE 128 AUTO', 'C128', 1),
(12, 'CODE 128 A', 'C128A', 1),
(13, 'CODE 128 B', 'C128B', 1),
(14, 'CODE 128 C', 'C128C', 1),
(15, 'EAN 8', 'EAN8', 1),
(16, 'EAN 13', 'EAN13', 1),
(17, 'UPC-A', 'UPCA', 1),
(18, 'UPC-E', 'UPCE', 1),
(19, 'QRCODE,L', 'QRCODE,L', 1),
(20, 'QRCODE,M', 'QRCODE,M', 1),
(21, 'QRCODE,Q', 'QRCODE,Q', 1),
(22, 'QRCODE,H', 'QRCODE,H', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`, `status`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1),
(2, 3, 4, 'ventas', '$2y$10$jnRK1TUrjmqgv//cnP12k.7Vj4yv12KTvSTNgbjRTiU5eCGSMEaiu', 1),
(3, 2, 3, 'admins', '$2y$10$VmB1NVnb0A9GCrBG1sDE9.RGhWahKkKkHRdImRKOlww2xBSLGe0qG', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `ventaId` bigint(20) NOT NULL,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `personalId` int(11) NOT NULL,
  `ClientesId` bigint(20) NOT NULL,
  `subtotal` float NOT NULL,
  `ndescuento` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL,
  `tipopago` int(1) NOT NULL DEFAULT '1' COMMENT '1 contado 2 credito',
  `fechavencimiento` date DEFAULT NULL COMMENT 'la fecha de vencimiento en caso de ser credito',
  `pagado` int(1) NOT NULL DEFAULT '1' COMMENT '1 pagado, 0 no cubierto en el caso de los creditos',
  `metodo` int(1) NOT NULL DEFAULT '1' COMMENT '1 efectivo, 2 credito, 3 debito',
  `cancelado` int(1) NOT NULL DEFAULT '0',
  `cancela_personal` int(11) DEFAULT NULL,
  `cancela_motivo` text,
  `canceladoh` timestamp NULL DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`ventaId`, `sucursalid`, `personalId`, `ClientesId`, `subtotal`, `ndescuento`, `descuento`, `total`, `tipopago`, `fechavencimiento`, `pagado`, `metodo`, `cancelado`, `cancela_personal`, `cancela_motivo`, `canceladoh`, `activo`, `reg`) VALUES
(1, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-06 01:51:04'),
(2, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:34:14'),
(3, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:37:27'),
(4, 1, 1, 1, 72, 0, 0, 72, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:39:10'),
(5, 1, 1, 1, 24, 0, 0, 24, 2, '2019-05-31', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-12 02:01:45'),
(6, 1, 1, 1, 72, 0.1, 7.2, 64.8, 2, '2019-05-27', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-27 03:31:48'),
(7, 1, 1, 1, 48, 0.05, 2.4, 45.6, 2, '0000-00-00', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-27 16:13:20'),
(8, 1, 1, 1, 216, 0.05, 10.8, 205.2, 2, '2019-05-29', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-29 15:49:31'),
(9, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-06-02 22:05:12'),
(10, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-06-02 22:57:53'),
(11, 1, 1, 1, 24, 0.05, 1.2, 22.8, 2, '2019-06-29', 0, 1, 0, NULL, NULL, NULL, 1, '2019-06-03 00:50:53'),
(12, 1, 1, 1, 6, 0, 0, 6, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-08-18 23:05:17'),
(13, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 00:35:40'),
(14, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 00:36:34'),
(15, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 17:22:57'),
(16, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 17:23:59'),
(17, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 17:24:34'),
(18, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-22 19:41:11'),
(19, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-25 21:16:25'),
(20, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-25 21:18:17'),
(21, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-25 21:21:09'),
(22, 1, 1, 1, 25, 0, 0, 25, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-09-25 21:48:34'),
(23, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-12 20:14:08'),
(24, 2, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-12 21:56:07'),
(25, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-26 13:09:04'),
(26, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-26 13:10:37'),
(27, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-26 13:24:29'),
(28, 1, 1, 1, 15.8, 0, 0, 15.8, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-04-26 15:57:42'),
(29, 1, 1, 1, 87.9, 0, 0, 87.9, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 17:32:01'),
(30, 1, 1, 1, 300, 0, 0, 300, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 17:43:37'),
(31, 1, 1, 7, 330, 0, 0, 330, 2, '2020-07-31', 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 17:46:47'),
(32, 1, 1, 1, 140, 0, 0, 140, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 17:55:58'),
(33, 1, 1, 1, 70, 0, 0, 70, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 18:34:55'),
(34, 1, 1, 1, 70, 0, 0, 70, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 18:38:57'),
(35, 1, 1, 1, 70, 0, 0, 70, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 20:41:36'),
(36, 1, 1, 1, 140, 0, 0, 140, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2020-07-04 20:44:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `ventadId` bigint(20) NOT NULL,
  `ventaId` bigint(20) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `precioc` float NOT NULL COMMENT 'es el precio de compra en el momento de que se realizo la venta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`ventadId`, `ventaId`, `productoid`, `cantidad`, `precio`, `precioc`) VALUES
(1, 1, 1, 1, 24, 23),
(2, 2, 1, 1, 24, 23),
(3, 3, 1, 1, 24, 23),
(4, 4, 1, 3, 24, 23),
(5, 5, 1, 1, 24, 23),
(6, 6, 1, 3, 24, 20),
(7, 7, 1, 2, 24, 20),
(8, 8, 1, 9, 24, 20),
(9, 9, 1, 1, 24, 20),
(10, 10, 1, 1, 24, 20),
(11, 11, 1, 1, 24, 20),
(12, 12, 3, 4, 1.5, 0.5),
(13, 13, 12, 1, 15.8, 10),
(14, 14, 12, 1, 15.8, 10),
(15, 15, 4, 1, 25, 25),
(16, 16, 4, 1, 25, 25),
(17, 17, 4, 1, 25, 25),
(18, 18, 4, 1, 25, 25),
(19, 19, 4, 1, 25, 25),
(20, 20, 4, 1, 25, 25),
(21, 21, 4, 1, 25, 25),
(22, 22, 4, 1, 25, 25),
(23, 23, 12, 1, 15.8, 10),
(24, 24, 12, 1, 15.8, 10),
(25, 25, 12, 1, 15.8, 10),
(26, 26, 12, 1, 15.8, 10),
(27, 27, 12, 1, 15.8, 10),
(28, 28, 12, 1, 15.8, 10),
(29, 29, 10, 1, 87.9, 80),
(30, 30, 13, 5, 60, 50),
(31, 31, 13, 5.5, 60, 50),
(32, 32, 13, 2, 70, 55),
(33, 33, 13, 1, 70, 55),
(34, 34, 13, 1, 70, 55),
(35, 35, 13, 1, 70, 55),
(36, 36, 13, 2, 70, 55);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `backup`
--
ALTER TABLE `backup`
  ADD PRIMARY KEY (`backupid`),
  ADD KEY `backup_fk_personal` (`personalId`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`bitacoraid`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`compraId`),
  ADD KEY `compra_fk_proveedor` (`id_proveedor`),
  ADD KEY `compra_fk_personal` (`personalId`),
  ADD KEY `compras_fk_sucursal` (`sucursalid`);

--
-- Indices de la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  ADD PRIMARY KEY (`compradId`),
  ADD KEY `compra_fk_compra` (`compraId`),
  ADD KEY `compra_fk_productos` (`productoid`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`gastosid`),
  ADD KEY `gastos_fk_sucursal` (`sucursal`),
  ADD KEY `gastos_fk_personal` (`personalId`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  ADD PRIMARY KEY (`pagoId`),
  ADD KEY `pago_fk_venta` (`ventaId`),
  ADD KEY `pago_fk_personal` (`personalId`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`),
  ADD KEY `personal_fk_sucursal` (`sucursalId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`),
  ADD KEY `producto_fk_categoria` (`categoria`);

--
-- Indices de la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_fk_sucursal` (`idproducto`),
  ADD KEY `prosuc_fk_sucursal` (`idsucursal`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`sucursalid`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `ticket_bar`
--
ALTER TABLE `ticket_bar`
  ADD PRIMARY KEY (`barId`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`ventaId`),
  ADD KEY `ventas_fk_personal` (`personalId`),
  ADD KEY `ventas_fk_clientes` (`ClientesId`),
  ADD KEY `ventas_fk_sucursal` (`sucursalid`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`ventadId`),
  ADD KEY `detallev_fk_productos` (`productoid`),
  ADD KEY `ventasd_fk_ventas` (`ventaId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `backup`
--
ALTER TABLE `backup`
  MODIFY `backupid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `bitacoraid` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `compraId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  MODIFY `compradId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `gastosid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  MODIFY `pagoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `sucursalid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ticket_bar`
--
ALTER TABLE `ticket_bar`
  MODIFY `barId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `ventaId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `ventadId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `backup`
--
ALTER TABLE `backup`
  ADD CONSTRAINT `backup_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compra_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compras_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  ADD CONSTRAINT `compra_fk_compra` FOREIGN KEY (`compraId`) REFERENCES `compras` (`compraId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `gastos_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `gastos_fk_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  ADD CONSTRAINT `pago_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `pago_fk_venta` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `perfil_fk_menu` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `perfil_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `personal_fk_sucursal` FOREIGN KEY (`sucursalId`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `producto_fk_categoria` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`categoriaId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  ADD CONSTRAINT `producto_fk_sucursal` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `prosuc_fk_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuario_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuario_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_fk_clientes` FOREIGN KEY (`ClientesId`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `detallev_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventasd_fk_ventas` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
