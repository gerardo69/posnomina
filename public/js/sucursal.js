var base_url=$('#base_url').val();
$(document).ready(function($) {
    $('.guardarsucursal').click(function(event) {
        var data = new FormData();
        if ($('#exampleInputFile')[0].files.length > 0) {
            var inputFileImage = document.getElementById('exampleInputFile');
            var file = inputFileImage.files[0]; 
            data.append('logo',file);
        }
        data.append('sucursalid',$('#sucursalid').val());
        data.append('sucursal',$('#sucursal').val());
        data.append('direccion',$('#direccion').val());
        data.append('telefono',$('#telefono').val());
        $.ajax({
            url:base_url+'Sucursal/add',
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                console.log(data);
                new PNotify({
                                  title: 'Hecho!',
                                  text: 'Guardado Correctamente',
                                  type: 'success',
                                  styling: 'bootstrap3'
                              });
                setInterval(function(){ 
                            location.href=base_url+'Sucursal';
                        }, 3000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                    console.log(data);
                    if (data.ok=='true') {
                        $(".fileinput").fileinput("clear");
                    }else{
                        toastr.error('Error', data.msg);
                    }
                  
                }
        });
    });   
});
/*
function modal_eliminar(id,name){
    $('#eliminar_modal') .modal();
   // $(".eliminar_modal").slideToggle()
    $("#sucursalid").val(id);
    $(".nom").html('<b>'+name+'</b>');
}
function boton_eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Sucursal/eliminar',
        data: {sucursalid:$("#sucursalid").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
          if (data >=1) {  
            new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            setInterval(function(){ 
                     $('.num_'+$("#sucursalid").val()).remove(); 
                  }, 1000);
            }
        }
    });
}
*/