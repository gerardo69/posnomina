function productoscompras(id){
    $('#modal_productos').modal();
    var base_url = $('#base_url').val();
    $.ajax({
                  type: 'POST',
                  url: base_url + 'Compras/compraproductos',
                  data: {
                      compraid: id
                  },
                  async: false,
                  statusCode: {
                      404: function(data) {
                          new PNotify({
                              title: 'Error!',
                              text: 'No Se encuentra el archivo',
                              type: 'error',
                              styling: 'bootstrap3'
                          });
                      },
                      500: function() {
                          new PNotify({
                              title: 'Error!',
                              text: 'Error 500',
                              type: 'error',
                              styling: 'bootstrap3'
                          });
                      }
                  },
                  success: function(data) {
                    $('.viewproductos').html('');
                    $('.viewproductos').html(data);
                    $('#tableproductoscompras').DataTable({
                      "lengthChange": false
                    });
                  }
              });
  }