function grafica() {
            
              
            if ($('#graficaventas').length ){ 
              
              var ctx = document.getElementById("graficaventas");
              var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                  labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre","Octubre", "Noviembre", "Diciembre"],
                  datasets: [{
                    label: '# of Votes',
                    backgroundColor: "#26B99A",
                    data: [51, 30, 40, 28, 92, 50, 45, 92, 50, 45, 45, 56]
                  }, {
                    label: '# of Votes',
                    backgroundColor: "#03586A",
                    data: [41, 56, 25, 48, 72, 34, 12, 56, 25, 48, 72, 34]
                  }]
                },

                options: {
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
              
            } 
              
} 


$(document).ready(function() {
            
    grafica();
            
});
