var base_url = $('#base_url').val();
$(document).ready(function() {
  $("#search").focus();
  $('.verificar_producto').click(function(event) {
     var codigo=$("#search").val();
     verificar(codigo);
  });
});
function verificar(codigob){
  $.ajax({
        type:'POST',
        url: base_url+'Verificador/verifica',
        data: {codigo:codigob},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            $('.tabla_verifica').html(data);
            $(".imgpro").css({"height":"100px","border-radius":"5px","box-shadow":"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"});
            setInterval(function(){ 
                    location.href= base_url+'Verificador'; 
                  }, 15000);
            
        }
    });
}