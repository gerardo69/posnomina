var base_url = $('#base_url').val();
var tipopago = 1;
var realisaventa = 0;
var elem = document.documentElement;
$(document).ready(function() {
    $('#menu_toggle').click();
    $('#clientes').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Cliente',
        ajax: {
            url: base_url + 'General/searchcliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var clientes = data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ClientesId,
                        text: element.Nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url + 'General/searchproducto',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var clientes = data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoid,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        addproducto();
    });
    $('#producto2').focus();
    $('input[name="tipopago"]').change(function() {
        tipopago = $('input[name="tipopago"]:checked').val();
        if (tipopago == 1) {
            $('.metodocontado').show("slow");
            $('.metodocredito').hide("slow");
            $('#pago').prop('readonly', false);
        } else {
            $('#tipopago option[value=1]').attr('selected', 'selected');
            $('.metodocontado').hide("slow");
            $('.metodocredito').show("slow");
            $('#pago').val(0);
            $('#pago').prop('readonly', true);
        }
    });
    $('#ndescuento').change(function(event) {
        calcular();
    });
    $('#metodo').change(function(event) {
        var metodo = $('#metodo option:selected').val();
        if (metodo > 1) {
            var addpago = $('#total').val();
            $('#pago').val(addpago);
            $('#pago').prop('readonly', true);
        } else {
            $('#pago').val(0);
            $('#pago').prop('readonly', false);
        }
    });
    $('.guardarventa').click(function(event) {
        if (tipopago == 2) {
            realisaventa = 1;
        } else {
            var totalv = $('#total').val();
            var pagov = $('#pago').val();
            if (pagov > 0) {
                if (parseFloat(pagov) >= parseFloat(totalv)) {
                    realisaventa = 1;
                } else {
                    realisaventa = 0;
                    new PNotify({
                        title: 'Advertencia!',
                        text: 'Debe de agregar una cantidad mayor o igual al total',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }

            } else {
                realisaventa = 0;
                new PNotify({
                    title: 'Advertencia!',
                    text: 'Debe de agregar una cantidad de pago',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
        if (realisaventa == 1) {
            $.ajax({
                type: 'POST',
                url: base_url + 'Ventas/ingresarventa',
                data: {
                    ClientesId: $('#clientes option:selected').val(),
                    subtotal: $('#subtotal').val(),
                    ndescuento: $('#ndescuento option:selected').val(),
                    descuento: $('#descuento').val(),
                    total: $('#total').val(),
                    tipopago: $('input[name="tipopago"]:checked').val(),
                    fechavencimiento: $('#fechavencimiento').val(),
                    metodo: $('#metodo option:selected').val(),
                    sucursal: $('#sucursalselected option:selected').val()
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        new PNotify({
                            title: 'Error!',
                            text: 'No Se encuentra el archivo',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    },
                    500: function() {
                        new PNotify({
                            title: 'Error!',
                            text: 'Error 500',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                },
                success: function(data) {
                    addproductosventas(data);
                }
            });
        }
    });
    $('.productosclear').click(function() {
        limpiar();
    });
    //vigenciasistema(); bloqueo en el caso de que se alga del tiempo de pago
    $('.productostactiles').click(function(event) {
        //$('#productos_modal').modal();
        $('.versiontouch').show("slow");
        viecategoria();    
    });    
    $('.calculadoramodal').click(function(event) {
        $('#modalcalculadora').modal();
    });
    $( "#producto2" ).keypress(function(e) {
        if (e.which==13) {
            var codigo=$('#producto2').val();
            buscaridpro(codigo);
        }
    });
    

});

function addproducto() {
    if ($('#cantidad').val() > 0) {
        $.ajax({
            type: 'POST',
            url: base_url + 'Ventas/addproducto',
            data: {
                cant: $('#cantidad').val(),
                prod: $('#producto').val()
            },
            async: false,
            statusCode: {
                404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                console.log(data);
                $('#class_productos').html(data);
                calcular();
            }
        });
        $('#cantidad').val(1);
        $('#producto').html('');
        $("#producto").val(0).change();
        //$('#producto').select2('open').on('focus');
        $('#producto2').html('');
        $('#producto2').focus();
    }
    //calculartotal();
}

function deletepro(id) {

    $.ajax({
        type: 'POST',
        url: base_url + 'Ventas/deleteproducto',
        data: {
            idd: id
        },
        async: false,
        statusCode: {
            404: function(data) {
                new PNotify({
                    title: 'Error!',
                    text: 'No se encuentra archivo',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            },
            500: function() {
                new PNotify({
                    title: 'Error!',
                    text: '500',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        },
        success: function(data) {
            $('.producto_' + id).remove();
            calcular();
        }
    });
}

function calcular() {
    var addtp = 0;
    $(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });

    $('#subtotal').val(addtp);
    var subtotal = addtp;

    var ndescuento = $('#ndescuento option:selected').val();

    var descuento = parseFloat(subtotal) * parseFloat(ndescuento);
    descuento = parseFloat(descuento).toFixed(2);
    $('#descuento').val(descuento);

    var total = parseFloat(subtotal) - parseFloat(descuento);

    $('#total').val(total);
    $('#totalinfo').val(new Intl.NumberFormat('es-MX').format(total));

    var pago = $('#pago').val();
    if (pago == '') {
        pago = 0;
    }
    pago = '' ? 0 : pago;
    var cambio = parseFloat(pago) - parseFloat(total);
    //cambio < 0 ? 0 : cambio.toFixed(2);
    if (cambio < 0) {
        cambio = 0;
    }else{
        cambio = cambio.toFixed(2);
    }
    $('#cambio').val(cambio);





    /*
    //var descuento1=100/$('#mdescuento').val();
    $('#vsbtotal').val(addtp);
    var descuento=addtp*$('#mdescuento').val();
    $('#cantdescuento').val(descuento);
    var total=parseFloat(addtp)-parseFloat(descuento);
    $('#vtotal').val(Number(total).toFixed(2));
    ingreso();*/
}

function addproductosventas(idventa) {
    var DATA = [];
    var TABLA = $("#productosv tbody > tr");
    TABLA.each(function() {
        item = {};
        item["ventaId"] = idventa;
        item["productoid"] = $(this).find("input[id*='vsproid']").val();
        item["cantidad"] = $(this).find("input[id*='vscanti']").val();
        item["precio"] = $(this).find("input[id*='vsprecio']").val();
        DATA.push(item);
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Ventas/ingresarventapro',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
            404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
        },
        success: function(data) {}
    });
    checkprint = document.getElementById("checkimprimir").checked;
    if (checkprint == true) {
        $('#modal_print').modal();
        var cambio = $('#cambio').val();
        $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_venta/' + idventa + '/'+cambio+'" class="iframeprint" id="iframeprint"></iframe>');
    } else {
        toastr.success('Venta Realizada', 'Hecho!');
    }
    limpiar();
    $("#vcliente").val(45).change();
    $('#vtotal').val(0);
    $('#vcambio').val(0);
    $('#vingreso').val(0);
    $('#pago').val(0);
    $('#cambio').val(0);
}
function limpiar(){
    $('#class_productos').html('');
        $.ajax({
            type: 'POST',
            url: base_url + 'Ventas/productoclear',
            async: false,
            statusCode: {
                404: function(data) {
                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                calcular();
            }
        });
}
function vigenciasistema(){
    $.ajax({
            type: 'POST',
            url: base_url + 'Ventas/vigencia',
            data: {fecha:$('#fecha_actual').val()},
            async: false,
            statusCode: {
                404: function(data) {
                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                var f_actual = $('#fecha_actual').val();
                var f_vigencia = $('#fecha_v').val();

                if(f_vigencia<f_actual){
                   new PNotify({
                        title: 'Vigencia!',
                        text: 'Se a terminado su servicio activelo de nuevo.',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                   bloqueo();
                   var perfilid = $('#perfilid').val();
                   if(perfilid==1){
                     setTimeout(function() {location.href=base_url+'Pagos';}, 3000); 
                     /*
                   }else if(perfilid==2){
                     setTimeout(function() {location.href=base_url+'Pagos';}, 3000); 
                     */
                   } else{
                     setTimeout(function() {location.href=base_url+'Ventas/Activar';}, 3000); 
                   }
                   
                }else{
                        if(data==4){
                           new PNotify({
                                title: 'Vigencia!',
                                text: 'Aviso de servicio proximo a vencer dentro de '+data+' dias',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }else if(data==3){
                           new PNotify({
                                title: 'Vigencia!',
                                text: 'Aviso de servicio proximo a vencer dentro de '+data+' dias',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }else if(data==2){
                           new PNotify({
                                title: 'Vigencia!',
                                text: 'Aviso de servicio proximo a vencer dentro de '+data+' dias',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }else if(data==1){
                            var fecha_vigencia = $('#fecha_vigencia').val();
                           new PNotify({
                                title: 'Vigencia!',
                                text: 'Aviso de servicio proximo a vencer dentro de '+data+' dias. Fecha: '+fecha_vigencia+'',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                        else if(data==0){
                            var fecha_vigencia = $('#fecha_vigencia').val();
                           new PNotify({
                                title: 'Vigencia!',
                                text: 'Aviso de servicio esta apunto de terminarse su suscripcion en la fecha y hora: '+fecha_vigencia+'',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                }
            }
        });
}
function bloqueo(){
    $("input").prop('disabled', true);
    $("textarea").prop('disabled', true);
    $("select").prop('disabled', true);
    $("checkbox").prop('disabled', true);
    $("button").prop('disabled', true);
}
function viewproduct(cat){
    $.ajax({
                type: 'POST',
                url: base_url + 'Ventas/viewproduct',
                data: {
                    categoria: cat
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        new PNotify({
                            title: 'Error!',
                            text: 'No Se encuentra el archivo',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    },
                    500: function() {
                        new PNotify({
                            title: 'Error!',
                            text: 'Error 500',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                },
                success: function(data) {
                     $('.versiontouch').html(data);
                }
            });
}
function buscaridpro(codigo){
    $.ajax({
            type: 'POST',
            url: base_url + 'General/barproducto',
            data: {
                prod: codigo
            },
            async: false,
            statusCode: {
                404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                console.log(data);
                var array = $.parseJSON(data);
                var pro = array.pro;
                var proid = array.proid;
                var prorow = array.prorow;
                if (pro==1) {
                    if (prorow==1) {
                        productselected(proid);
                    }else{
                        new PNotify({
                            title: 'Error!',
                            text: 'Codigo duplicado',
                            type: 'error',
                            styling: 'bootstrap3'
                        });  
                    }

                }else{
                    new PNotify({
                        title: 'Error!',
                        text: 'No existe producto',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }

            }
        }); 
}
function productselected(id){
    var cantidad=$('#cantidad').val()==''?1:$('#cantidad').val();
   $.ajax({
            type: 'POST',
            url: base_url + 'Ventas/addproducto',
            data: {
                cant: cantidad,
                prod: id
            },
            async: false,
            statusCode: {
                404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                console.log(data);
                $('#class_productos').html(data);
                calcular();
                $('#cantidad').val(1);
                $('#producto2').val('');
                $('#producto2').focus();
            }
        }); 
}
function viecategoria(){
    $.ajax({
            type: 'POST',
            url: base_url + 'Ventas/viewpeocat',
            async: false,
            statusCode: {
                404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                $('.versiontouch').html(data);
            }
        });
}
function tactilcerrar(){
    $(".versiontouch").hide( "slow" );
}
function fullscreen(){
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
    $('.fullscreen').removeAttr('onclick');
    $('.fullscreen').attr('onClick', 'exitfullscreen();');
}
function exitfullscreen(){
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
    $('.fullscreen').removeAttr('onclick');
    $('.fullscreen').attr('onClick', 'fullscreen();');
}
function imprimiriframa(documentId){
    document.getElementById(documentId).contentWindow.print();
}