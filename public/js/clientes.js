var base_url = $('#base_url').val();
$(document).ready(function () {
	$( '#descripcionc' ).ckeditor();
		var form_register = $('#formclientes');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Nombre:{
                  required: true
                },
                
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //=====================================================================================================

        $('.guardar').click(function(event) {
            var $valid = form_register.valid();
            console.log($valid);
            if($valid) {
                var datos = form_register.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Clientes/add',
                    data: datos,
                    async: false,
                    statusCode:{
                        404: function(data){
                            new PNotify({
                                  title: 'Error!',
                                  text: 'No se encuentra archivo',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });

                        },
                        500: function(){
                            new PNotify({
                                  title: 'Error!',
                                  text: '500',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                        }
                    },
                    success:function(data){
                        new PNotify({
                                  title: 'Hecho!',
                                  text: 'Guardado Correctamente',
                                  type: 'success',
                                  styling: 'bootstrap3'
                              });
                        setInterval(function(){ 
                        location.href=base_url+'Clientes';
                        }, 3000);

                    }
                });
            }
        });

        $('#data-tables').DataTable({
            fixedHeader: true,
            responsive: !0,
            "order": [[ 0, "desc" ]],
            "paging": false,
            "info": false,
            "lengthChange": false,
            "searching": false
        }); 
});


function modal_eliminar(id,name){
    $('#eliminar_modal') .modal();
   // $(".eliminar_modal").slideToggle()
    $("#ClientesId").val(id);
    $(".nom").html('<b>'+name+'</b>');
}
function boton_eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/eliminar',
        data: {ClientesId:$("#ClientesId").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            setInterval(function(){ 
                   $('.num_'+$("#ClientesId").val()).remove(); 
                }, 1000);
            
        }
    });
}