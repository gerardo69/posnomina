<style type="text/css">
  .iframeprint{
    width: 100%;
    height: 76vh;
    border: 0;
  }
  .iframeticket{
    padding: 0px;
  }
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Nueva Compra</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Compra <small><input type="checkbox" name="checkimprimir" id="checkimprimir" checked> <label for="checkimprimir"> Imprimir Ticket</label></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <label class="control-label col-md-12 col-sm-12 col-xs-12">Proveedor</label>
                      <select class="form-control" id="id_proveedor" name="id_proveedor"></select>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                      <label>Sucursal</label>
                      <select name="sucursalselected" class="form-control" id="sucursalselected">
                        <?php foreach ($sucursalesrow->result() as $row) { ?>
                            <option value="<?php echo $row->sucursalid;?>"><?php echo $row->sucursal?></option>
                        <?php } ?>
                      </select>
                    </div> 
                    
                   
                  </div>
                  <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                      <label>Cantidad</label>
                      <input type="number" name="cantidadp" id="cantidadp" class="form-control">
                      
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <label>Codigo/producto</label>
                      <select class="form-control" id="producto" name="producto"></select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                      <label>Precio Compra</label>
                      <input type="number" name="precioc" id="precioc" class="form-control">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <button  class="btn btn-info addproducto">Agregar producto</button>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-xs-12">
                      <table class="table table-hover" id="productosc">
                        <thead>
                          <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio Unitario</th>
                            <th>Precio Total</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="class_productos">
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Total</label>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                      <input type="number" name="monto_total" id="monto_total" class="form-control" readonly>
                     

                    </div>
                  </div>
                  
                </div>
              </div>
              



                

               

                  
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>Compras">Cancel</a>
                    <button  class="btn btn-success limpiar">Limpiar</button>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12">

                    <button  class="btn btn-success guardar">Guardar</button>
                  </div>
                </div>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modal_print">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Impresion</h3>
      </div>
      <div class="modal-body iframereporte">
        
      </div>
      <div class="modal-footer  curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-default" onclick="imprimiriframa('iframeprint')">Imprimir</button>      
      </div>

    </div>
  </div>
</div>  