<style type="text/css">
  .imgpro{
      height: 100px;
      border-radius: 5px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  }
  .imgpro:hover{
    transform: scale(1.5);
  }
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Verificar <small>Precios</small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Producto<small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form >
                  <div class="form-group">
                    <div class="col-md-2 col-sm-2 col-xs-12"></div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <div class="input-group">
                        <input type="text" class="form-control" name="search" id="search" placeholder="Escaneé o tecleé código de barras del producto">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary verificar_producto">Buscar</button>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12"></div>
                  </div>
              </form>
          <div class="alert alert-info alert-dismissible fade in" role="alert">
            <strong>Resultados de la búsqueda</strong>
          </div>
          <div class="tabla_verifica"></div> 
              <hr>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    