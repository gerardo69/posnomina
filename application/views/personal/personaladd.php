<!-- page content -->
<div class="right_col" role="main">
  
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Personal <small><?php echo $subtitle;?></small></h3>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          <div class="row">
            <form method="post"  role="form" id="formpersonal">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <h3>Datos personales</h3>
                <hr />
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nombre del empleado:</label>
                  <div class="col-md-10 col-sm-9 col-xs-12 controls">
                    <input type="text" name="nombre" class="form-control has-feedback-left" id="nombre" value="<?php echo $nombre;?>" >
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    <input type="text" placeholder="Username" id="personalId" value="<?php echo $id; ?>" name="personalId" hidden>
                  </div>
                </div>
                
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <h3>Datos de contacto</h3>
                <hr />
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="col-md-2 col-sm-2 col-xs-12 control-label">Domicilio:</label>
                  <div class="col-md-10 col-sm-10 col-xs-12 controls">
                    <input type="text" name="domicilio" id="domicilio" class="form-control has-feedback-left" value="<?php echo $domicilio;?>">
                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="col-md-2 col-sm-2 col-xs-12 control-label">Teléfono local:</label>
                  <div class="col-md-3 col-sm-3 col-xs-12 controls">
                    <input type="text" id="telefono" name="telefono" value="<?php echo $telefono;?>" class="form-control has-feedback-left"/>
                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <label class="col-md-2 col-sm-2 col-xs-12 control-label">Teléfono celular:</label>
                  <div class="col-md-5 col-sm-5 col-xs-12 controls">
                    <input type="text" id="celular" name="celular" value="<?php echo $celular;?>" class="form-control has-feedback-left"/>
                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  
                  <label class="col-md-2 col-sm-2 col-xs-12 control-label">Email:</label>
                  <div class="col-md-3 col-sm-3 col-xs-12 controls">
                    <input type="email" id="correo" name="correo" value="<?php echo $correo;?>" class="form-control has-feedback-left"/>
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <hr />
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12" <?php echo $sucursalview;?> >
                <div class="form-group">
                  <label class="col-md-2 col-sm-2 col-xs-12 control-label">Sucursal:</label>
                  <div class="col-md-3 col-sm-3 col-xs-12 controls">
                    <select name="sucursalId" id="sucursalId" class="form-control has-feedback-left">
                      <?php foreach ($sucursales->result() as $item) { ?>
                        <option value="<?php echo $item->sucursalid;?>" <?php if ($sucursalId==$item->sucursalid){ echo 'selected';} ?> ><?php echo $item->sucursal;?></option>
                      <?php } ?>
                      
                    </select>
                    <span class="fa fa-building form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              
              
            </form>
            <div class="col-md-3 col-md-offset-9">
              <button class="btn btn-primary">Cancelar</button>
              <button class="btn btn-success"  type="submit"  id="savep"><?php echo $button;?></button>
            </div>
            
          </div>
          
          
          <!--------//////////////-------->
          
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <br />
</div>
<!-- /page content -->