<style type="text/css">
  .imgpro{
      height: 100px;
      border-radius: 5px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  }
  .imgpro:hover{
    transform: scale(1.5);
  }
</style>
<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Sucursal <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          
          <div class="row">
            <div class="col-md-8 text-left">
            </div>
            <div class="col-md-4">
              <div class="row text-right">
                <!--
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Sucursal/">Nueva sucursal</a>
                -->
              </div>
              <div class="row">
                <form action="<?php echo base_url(); ?>Sucursal">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Buscar..." value="<?php echo $buscar;?>">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default" type="button">Buscar</button>
                    </span>
                  </div>
                </form>
              </div>
              
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data-tables">
            <thead>
              <tr >
                <th>#</th>
                <th></th>
                <th>Sucursal</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              <?php foreach ($Sucursal->result() as $item){ ?>
                <tr class="num_<?php echo $item->sucursalid; ?>">
                  <td><?php echo $item->sucursalid; ?></td>
                  <td><?php 
                          if ($item->logo!='') {
                            $logo='public/images/sucursalt/'.$item->logo;
                          }
                      ?>
                    <img src="<?php echo $logo; ?>" class="imgpro">
                  </td>
                  <td><?php echo $item->sucursal; ?></td>
                  <td><?php echo $item->direccion; ?></td>
                  <td><?php echo $item->telefono; ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>
                      <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="<?php echo base_url(); ?>Sucursal/Sucursaladd/<?php echo $item->sucursalid; ?>">Editar</a>
                        </li>
                        <!--
                        <li>
                          <a onclick="modal_eliminar(<?php echo $item->sucursalid; ?>,'<?php echo $item->sucursal; ?>')">Eliminar</a>
                        </li>
                        -->
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              
            </tbody>
          </table>
          <div class="col-md-12">
            <div align="right">
              <?php echo $this->pagination->create_links() ?>
            </div>
          </div>
   

          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="eliminar_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header alert-danger curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro de que desea eliminar la sucursal <b class="nom"></b>?</h4>
        <input type="hidden" name="sucursalid" id="sucursalid">
        <br>
      </div>
      <div class="modal-footer alert-danger curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="boton_eliminar()">Aceptar</button>
      </div>
    </div>
  </div>
</div>   