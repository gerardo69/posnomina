<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Proveedores <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////-------->
          
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
              <div class="row text-right">
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Proveedores/Proveedoresadd">Nuevo</a>
              </div>
              <div class="row">
                <form action="<?php echo base_url(); ?>Proveedores">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Buscar...">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default" type="button">Buscar</button>
                    </span>
                  </div>
                </form>
              </div>
              
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th style="width: 100px;">Razon social</th>
                <th>Domicilio</th>
                <th>Ciudad</th>
                <th>Telefono local</th>
                <th>Telefono celular</th>
                <th>Email</th>
                <th>RFC</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($Proveedores->result() as $item){ ?>
                <tr class="num_<?php echo $item->id_proveedor; ?>">
                  <td><?php echo $item->id_proveedor; ?></td>
                  <td><?php echo $item->razon_social; ?></td>
                  <td><?php echo $item->domicilio; ?></td>
                  <td><?php echo $item->ciudad; ?></td>
                  <td><?php echo $item->telefono_local; ?></td>
                  <td><?php echo $item->telefono_celular; ?></td>
                  <td><?php echo $item->email_contacto; ?></td>
                  <td><?php echo $item->rfc; ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>
                      <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="<?php echo base_url(); ?>Proveedores/Proveedoresadd/<?php echo $item->id_proveedor; ?>">Editar</a>
                        </li>
                        <li>
                          <a onclick="modal_eliminar(<?php echo $item->id_proveedor; ?>,'<?php echo $item->razon_social; ?>')">Eliminar</a>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              
            </tbody>

          </table>
          <div class="col-md-12">
            <div align="right">
              <?php echo $this->pagination->create_links() ?>
            </div>
          </div>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="eliminar_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header alert-danger curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro de que desea eliminar el proveedor <b class="nom"></b>?</h4>
        <input type="hidden" name="id_proveedor" id="id_proveedor">
        <br>
      </div>
      <div class="modal-footer alert-danger curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="boton_eliminar()">Aceptar</button>
      </div>
    </div>
  </div>
</div>    