<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Pagos <small></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Pagos <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                  <span class="count_top"><i class="fa fa-user"></i> Usuarios Máximos</span>
                  <div class="count"><?php echo $usuariosmaximo;?></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                  <span class="count_top"><i class="fa fa-shopping-cart"></i> Productos Máximos</span>
                  <div class="count"><?php echo $productosmaximos;?></div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 pricing">
                  <div class="title">
                              <h2>Vencimiento licencia</h2>
                              <h1><?php echo date('d/m/Y',strtotime($vigencia)) ;?></h1>
                            </div>
                  
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                  <span class="count_top"><i class="fa fa-credit-card"></i> Costo mensual</span>
                  <div class="count"><?php echo $costomensual;?></div>
                  <a href="https://compropago.com/comprobante/?id=034ba5ef-aaef-4325-8d7f-94a90e40d1db" target="_blank" class="btn btn-success">Pago Mensual</a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                  <span class="count_top"><i class="fa fa-credit-card"></i> Costo anual</span>
                  <div class="count"><?php echo $costoanual;?></div>
                  <a href="https://compropago.com/comprobante/?id=e9df9c49-15d9-40d5-9e8d-48c9a9754f56" target="_blank" class="btn btn-success">Pago Anual</a>
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <p>Para Usuarios y/o Sucursales adicionales Contactar a un asesor <a href="#">aquí <i class="fa fa-comments"></i> </a></p>

                </div>
                

              </div>
              

            </div>
          </div>
        </div>
    </div>

  </div>
</div>    