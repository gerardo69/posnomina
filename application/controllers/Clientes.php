<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,6);// 6 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
        //====================================
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }
            $data['buscar']=$buscar;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Clientes/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_clientes($buscar);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Clientes"] = $this->ModeloCatalogos->List_table_clientes($pagex,$config['per_page'],$buscar);
        //====================================
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/clienteslista',$data);
        $this->load->view('templates/footer');
        $this->load->view('clientes/clienteaddjs');
        
	}
    function Clienteadd($id=0){
        
        if ($id==0) {
            $data['subtitle']='Nuevo';
            $data['button']='Guardar';
            
            $data['ClientesId'] = $id;
            $data['Nombre'] = '';
            $data['Domicilio'] = '';
            $data['Ciudad'] = '';
            $data['Estado'] = '';
            $data['CodigoPostal'] = '';
            $data['Correo'] = '';
            $data['nombrec'] = '';
            $data['correoc'] = '';
            $data['telefonoc'] = '';
            $data['extencionc'] = '';
            $data['celular'] = '';
            $data['descripcionc'] = '';
            
        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('clientes','ClientesId',$id);
            foreach ($result->result() as $row) {
                $data['ClientesId'] = $row->ClientesId;
                $data['Nombre'] = $row->Nombre;
                $data['Domicilio'] = $row->Domicilio;
                $data['Ciudad'] = $row->Ciudad;
                $data['Estado'] = $row->Estado;
                $data['CodigoPostal'] = $row->CodigoPostal;
                $data['Correo'] = $row->Correo;
                $data['nombrec'] = $row->nombrec;
                $data['correoc'] = $row->correoc;
                $data['telefonoc'] = $row->telefonoc;
                $data['extencionc'] = $row->extencionc;
                $data['celular'] = $row->celular;
                $data['descripcionc'] = $row->descripcionc;
            }
            $data['subtitle']='Editar';
            $data['button']='Actualizar';
            
        }
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/clienteadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('clientes/clienteaddjs');
    }
    function add(){
        $data = $this->input->post();
        //echo $data;
        $id=$data['ClientesId'];
        unset($data['ClientesId']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('clientes',$data,'ClientesId',$id);
        }else{
            //echo $data;
            $this->ModeloCatalogos->Insert('clientes',$data);
        }
    }

    public function eliminar(){
        $id = $this->input->post('ClientesId');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('clientes',$data,'ClientesId',$id);
    }
    

}