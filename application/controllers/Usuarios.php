<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloUsuarios');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,19);// 19 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }

    }
	public function index(){
        $data['personal']=$this->ModeloUsuarios->personal($this->sucursalId);
        $data['perfiles']=$this->ModeloUsuarios->perfiles($this->sucursalId);
        $data['usuarios']=$this->ModeloUsuarios->usuarios($this->sucursalId);
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/usuarios',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/usuariosjs');
	}
    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->ModeloCatalogos->getselectvalue1rowwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result->result() as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    function add(){
        $data = $this->input->post();
        $id=$data['UsuarioID'];
        unset($data['UsuarioID']);
        $pass=$data['contrasena'];
        unset($data['contrasenav']);
        if ($id==0) {
            $tusu=$this->ModeloUsuarios->numusuarios();
            $tusupermitidos=$this->ModeloUsuarios->numusuariospermitidos();
            if ($tusu<$tusupermitidos) {
                $pass = password_hash($pass, PASSWORD_BCRYPT);
                $data['contrasena']=$pass;
                $this->ModeloCatalogos->Insert('usuarios',$data);
                $result=1;
            }else{
                $result=2;
            }
            
        }else{
            if ($pass=='xxxxxx') {
                unset($data['contrasena']);
            }else{
                $pass = password_hash($pass, PASSWORD_BCRYPT);
                $data['contrasena']=$pass;
            }
            $this->ModeloCatalogos->updateCatalogo('usuarios',$data,'UsuarioID',$id);
            $result=1;
        }
        echo $result;
    }
    function delete(){
        $id = $this->input->post('usu');
        $this->ModeloCatalogos->deleteCatalogo('usuarios','UsuarioID',$id);
    }
   



}