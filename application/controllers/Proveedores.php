<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,7);// 7 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }

	public function index(){
        //====================================
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }
            $data['buscar']=$buscar;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Proveedores/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_proveedores($buscar);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Proveedores"] = $this->ModeloCatalogos->List_table_proveedores($pagex,$config['per_page'],$buscar);

    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedoreslist',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/jsproveedores');
	}

	public function Proveedoresadd($id=0){
        if ($id==0) {
            $data['subtitle']='Nuevo';
            $data['button']='Guardar';
            
            $data['id_proveedor'] = $id;
            $data['razon_social'] = '';
            $data['domicilio'] = '';
            $data['ciudad'] = '';
            $data['estado'] = '';
            $data['cp'] = '';
            $data['telefono_local'] = '';
            $data['telefono_celular'] = '';
            $data['contacto'] = '';
            $data['email_contacto'] = '';
            $data['rfc'] = '';
            $data['obser'] = '';
        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('proveedores','id_proveedor',$id);
            foreach ($result->result() as $row) {
                $data['id_proveedor'] = $row->id_proveedor;
                $data['razon_social'] = $row->razon_social;
                $data['domicilio'] = $row->domicilio;
                $data['ciudad'] = $row->ciudad;
                $data['estado'] = $row->estado;
                $data['cp'] = $row->cp;
                $data['telefono_local'] = $row->telefono_local;
                $data['telefono_celular'] = $row->telefono_celular;
                $data['contacto'] = $row->contacto;
                $data['email_contacto'] = $row->email_contacto;
                $data['rfc'] = $row->rfc;
                $data['obser'] = $row->obser;
            }
            $data['subtitle']='Editar';
            $data['button']='Actualizar';
            
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedoresadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/jsproveedores');
	}

    public function add(){
        $data = $this->input->post();
        $id=$data['id_proveedor'];
        unset($data['id_proveedor']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('proveedores',$data,'id_proveedor',$id);
        }else{
            $this->ModeloCatalogos->Insert('proveedores',$data);
        }
    }

    public function eliminar(){
        $id = $this->input->post('id_proveedor');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('proveedores',$data,'id_proveedor',$id);
    }

}