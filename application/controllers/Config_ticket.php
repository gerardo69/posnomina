<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_ticket extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,13);// 13 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
     
    public function index(){

    	$ticket= $this->ModeloCatalogos->tickeall();
    	foreach ($ticket->result() as $item) {
            $data['id_ticket']=$item->id_ticket;
            $data['titulo']=$item->titulo;
            $data['mensajea']=$item->mensajea;
            $data['mensajeb']=$item->mensajeb;
            $data['fuente']=$item->fuente;
            $data['margen_superior']=$item->margen_superior;
            $data['tamanio']=$item->tamanio;
            $data['barcoderow']=$item->barcode;
            $data['anchot']=$item->anchot;
            
    	}
        $where=array('activo'=>1);
        $data['barcode']=$this->ModeloCatalogos->getselectvalue1rowwheren('ticket_bar',$where);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/ticket',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/jsticket');
    }
    public function updateticket(){
       $data = $this->input->post();
       $id=$data['id_ticket'];
       unset($data['id_ticket']);
       $this->ModeloCatalogos->updateCatalogo('ticket',$data,'id_ticket',$id);
    }


}